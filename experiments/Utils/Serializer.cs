﻿using System.IO;
using System.Xml.Serialization;

namespace Experiments.Utils
{
    public interface ISerializer
    {
        void Serialize<T>(T list);
        T Deserialize<T>();
    }

    public class Serializer : ISerializer
    {
        private string _file { get; set; }

        public Serializer()
        {
            _file = "data.xml";
        }

        public void Serialize<T>(T list)
        {
            if (File.Exists(_file)) { File.Delete(_file); }

            XmlSerializer serializer = new XmlSerializer(typeof(T));

            using (TextWriter writer = new StreamWriter(_file))
            {
                serializer.Serialize(writer, list);
            }  
        }

        public T Deserialize<T>()
        {
            XmlSerializer deserializer = new XmlSerializer(typeof(T));
            TextReader reader = new StreamReader(_file);
            object obj = deserializer.Deserialize(reader);
            var xmlData = (T)obj;
            reader.Close();

            return xmlData;
        }
    }
}