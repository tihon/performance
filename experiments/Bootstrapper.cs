﻿using Experiments.Entities;
using Experiments.Logic;
using Experiments.Services;
using Experiments.Utils;
using Microsoft.Practices.ServiceLocation;
using Microsoft.Practices.Unity;

namespace Epam.BackTest.App
{
    public static class Bootstrapper
    {
        public static UnityContainer Container { get; set; }

        public static void Run()
        {
            RegisterUnityContainer();
        }

        private static void RegisterUnityContainer()
        {
            Container = new UnityContainer();
            Container.RegisterInstance<IUnityContainer>(Container);

            var serviceLocator = new UnityServiceLocator(Container);
            Container.RegisterInstance<IServiceLocator>(serviceLocator);

            RegisterComponents();
            RegisterServices();
            RegisterLogics();

            ServiceLocator.SetLocatorProvider(() => serviceLocator);
        }

        private static void RegisterServices()
        {
            Container.RegisterType<IWorker, Worker>();
        }

        private static void RegisterLogics()
        {
            // Container.RegisterType<ILogic, LogicOne>();
            // Container.RegisterType<ILogic, LogicTwo>();
            // Container.RegisterType<ILogic, LogicThree>();
            Container.RegisterType<ILogic, LogicFour>();
        }

        private static void RegisterComponents()
        {
            Container.RegisterType<IDataGenerator, DataGenerator>(new ContainerControlledLifetimeManager());
            Container.RegisterType<ISerializer, Serializer>();
        }
    }
}