﻿using System;
using System.Collections.Generic;
using experiments;

namespace Experiments.Logic
{
    public interface ILogic
    {
        decimal Compute(List<Data> data, string company, string fieldName, DateTime startDate, DateTime endDate);
    }
}