﻿using System;
using System.Collections.Generic;
using System.Linq;
using experiments;
using Experiments.Logic;
using Experiments.Services;

public class LogicOne : ILogic
{
    private IWorker _worker;

    public LogicOne(IWorker worker)
    {
        _worker = worker;
    }

    public decimal Compute(List<Data> data, string company, string fieldName, DateTime startDate, DateTime endDate)
    {
        ComplicatedLogic(data, company, fieldName, startDate, endDate);

        // do some work

        ComplicatedLogic(data, company, fieldName, startDate.AddDays(1), endDate.AddDays(-1));

        // do some work again

        ComplicatedLogic(data, company, fieldName, startDate, endDate);

        return 0;
    }

    /// <summary>
    /// Do some operation for the specified date range for specified company and field + walking throue collection
    /// </summary>
    private void ComplicatedLogic(List<Data> data, string company, string fieldName, DateTime startDate, DateTime endDate)
    {
        var daysCount = (endDate - startDate).TotalDays;
        var currentDate = startDate;

        for (int i = 0; i < daysCount; i++)
        {
            // some work
            _worker.Work();
            
            // walking
            var randomItem = data.FirstOrDefault(d => d.Date == startDate.AddDays(ComplicatedLogic()));
            
            // get company
            var currentItems = data.FirstOrDefault(d => d.Date == currentDate && d.Name.ToUpper() == company.ToUpper());

            // get item
            if (currentItems != null)
            {
                var value = this.GetFieldValue(currentItems.Fields, fieldName);
            }

            currentDate = currentDate.AddDays(1);
        }
    }

    private double ComplicatedLogic()
    {
        return Faker.RandomNumber.Next(10);
    }

    private decimal GetFieldValue(List<Field> fields, string fieldName)
    {
        var field = fields.FirstOrDefault(f => f.Name.ToUpper() == fieldName.ToUpper());
        return field == null ? 0 : field.Value;
    }
}