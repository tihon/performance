﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using experiments;
using Experiments.Logic;
using Experiments.Services;

public class LogicFour : ILogic
{
    private IWorker _worker;

    Dictionary<DateTime, Dictionary<string, Data>> _transformedData;

    public LogicFour(IWorker worker)
    {
        _worker = worker;
    }

    public decimal Compute(List<Data> data, string company, string fieldName, DateTime startDate, DateTime endDate)
    {
        _transformedData = this.DataTransformation(data);

        ComplicatedLogic(data, company, fieldName, startDate, endDate);

        // do some work

        ComplicatedLogic(data, company, fieldName, startDate.AddDays(1), endDate.AddDays(-1));

        // do some work again

        ComplicatedLogic(data, company, fieldName, startDate, endDate);

        return 0;
    }

    private void ComplicatedLogic(List<Data> data, string company, string fieldName, DateTime startDate, DateTime endDate)
    {
        var daysCount = (endDate - startDate).TotalDays;
        var currentDate = startDate;

        var parallelOptions = new ParallelOptions();
        parallelOptions.MaxDegreeOfParallelism = System.Environment.ProcessorCount;

        Parallel.For(0, (int)daysCount, parallelOptions, i => 
        {
            _worker.Work();

            var randomDate = startDate.AddDays(Faker.RandomNumber.Next(10));
            var randomItem = _transformedData.ContainsKey(randomDate) ? _transformedData[randomDate] : null;

            var currentItem = _transformedData.ContainsKey(currentDate) ? _transformedData[currentDate][company.ToUpper()] : null;

            if (currentItem != null)
            {
                var value = this.GetFieldValue(currentItem.Fields, fieldName);
            }

            currentDate = currentDate.AddDays(1);
        });
    }

    private Dictionary<DateTime, Dictionary<string, Data>> DataTransformation(List<Data> data)
    {
        Dictionary<DateTime, Dictionary<string, Data>> transformedData = new Dictionary<DateTime, Dictionary<string, Data>>();

        foreach (var item in data)
        {
            if (!transformedData.Keys.Contains(item.Date))
            {
                transformedData.Add(item.Date, new Dictionary<string, Data>() { { item.Name, item } });
            }
            else
            {
                transformedData[item.Date].Add(item.Name, item);
            }
        }

        return transformedData;
    }

    private decimal GetFieldValue(List<Field> fields, string fieldName)
    {
        var field = fields.FirstOrDefault(f => f.Name.ToUpper() == fieldName.ToUpper());
        return field == null ? 0 : field.Value;
    }
}