﻿using System;
using System.Collections.Generic;
using System.Linq;
using experiments;
using Experiments.Logic;
using Experiments.Services;

public class LogicTwo : ILogic
{
    private IWorker _worker;

    public LogicTwo(IWorker worker)
    {
        _worker = worker;
    }

    public decimal Compute(List<Data> data, string company, string fieldName, DateTime startDate, DateTime endDate)
    {
        ComplicatedLogic(data, company, fieldName, startDate, endDate);

        // do some work

        ComplicatedLogic(data, company, fieldName, startDate.AddDays(1), endDate.AddDays(-1));

        // do some work again

        ComplicatedLogic(data, company, fieldName, startDate, endDate);

        return 0;
    }

    private void ComplicatedLogic(List<Data> data, string company, string fieldName, DateTime startDate, DateTime endDate)
    {
        var daysCount = (endDate - startDate).TotalDays;
        var currentDate = startDate;

        foreach (var currentItem in data.OrderBy(d => d.Date).Where(d => d.Date >= startDate && d.Date <= endDate && d.Name.Equals(company, StringComparison.InvariantCultureIgnoreCase)))
        {
            _worker.Work();

            var randomItem = data.FirstOrDefault(d => d.Date == startDate.AddDays(Faker.RandomNumber.Next(10)));
            
            var value = this.GetFieldValue(currentItem.Fields, fieldName);
        }
    }

    private decimal GetFieldValue(List<Field> fields, string fieldName)
    {
        var field = fields.FirstOrDefault(f => f.Name.Equals(fieldName, StringComparison.CurrentCultureIgnoreCase));
        return field == null ? 0 : field.Value;
    }
}