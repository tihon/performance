﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Experiments.Services
{
    public interface IWorker
    {
        void Work();
    }

    public class Worker : IWorker
    {
        public void Work()
        {
            Thread.Sleep(1);
        }
    }
}
