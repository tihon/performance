﻿using System;
using System.Diagnostics;
using Epam.BackTest.App;
using Experiments.Entities;
using Experiments.Logic;
using Experiments.Utils;
using Microsoft.Practices.ServiceLocation;

namespace experiments
{
    class Program
    {
        // settings
        private static int _month = 30;
        private static bool _regenerateData = false;

        // private fields
        private static Stopwatch _stopwatch = new Stopwatch();
        private static ISerializer _serializer;
        
        static void Main(string[] args)
        {
            Bootstrapper.Run();

            _serializer = ServiceLocator.Current.GetInstance<ISerializer>();

            if (_regenerateData) { FillTheData(); return; }

            StartWatch("Load data");
            var model = _serializer.Deserialize<Model>();
            LogTimeSpent("Load data");

            var computer = ServiceLocator.Current.GetInstance<ILogic>();

            StartWatch("Calculation");
            var result = computer.Compute(model.Data, "msft", "px_last", new DateTime(2013, 1, 1), new DateTime(2015, 1, 1));
            LogTimeSpent("Calculation");

            Console.WriteLine("Result: " + result);
        }

        private static void FillTheData()
        {
            var dataGenerator = ServiceLocator.Current.GetInstance<IDataGenerator>();
            _serializer.Serialize<Model>(dataGenerator.GeneratedataSet(_month));
        }

        public static void LogTimeSpent(string message)
        {
            _stopwatch.Stop();
            Console.WriteLine(message + " took : " + _stopwatch.Elapsed.Seconds + ":" + _stopwatch.Elapsed.Milliseconds);
            Console.WriteLine();
            _stopwatch.Reset();
        }

        public static void StartWatch(string message)
        {
            Console.WriteLine(message + "...");
            _stopwatch.Start();
        }
    }
}
