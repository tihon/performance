﻿using System;
using System.Collections.Generic;
using experiments;

namespace Experiments.Entities
{
    public interface IDataGenerator
    {
        Model GeneratedataSet(int count);
    }

    public class DataGenerator : IDataGenerator
    {
        public Model GeneratedataSet(int month)
        {
            var model = new Model();

            var now = DateTime.Now;

            var startDate = now.AddYears(-month);

            var currentDate = startDate;

            while (currentDate < now)
            {
                model.Data.AddRange(GenerateDataItemForDate(currentDate));

                currentDate = currentDate.AddDays(1);
            }

            return model;
        }

        private List<Data> GenerateDataItemForDate(DateTime currentDate)
        {
            var data = new List<Data>();

            for (int i = 0; i < ConstantsHolder.Securities.Length; i++)
            {
                data.Add(new Data
                {
                    Name = ConstantsHolder.Securities[i],
                    Date = currentDate.Date,
                    Fields = GenrateFields()
                });
            }

            return data;
        }

        private List<Field> GenrateFields()
        {
            return new List<Field>
            {
                GenerateFieldByName("px_last"),
                GenerateFieldByName("volume"),
                GenerateFieldByName("pe"),
                GenerateFieldByName("marketcap")
            };
        }

        private Field GenerateFieldByName(string fieldName)
        {
            return new Field
            {
                Name = fieldName,
                Value = Faker.RandomNumber.Next(1, 10000)
            };
        }
    }

    public static class ConstantsHolder
    {
        public static string[] Securities = { "MSFT", "AAPL", "EPAM", "TWTR", "FSBK", "PCCW", "INTL", "IBM", "AMD", "WV" };
    }
}