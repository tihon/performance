﻿using System;
using System.Collections.Generic;
using System.Security.Policy;
using System.Xml.Serialization;
using Experiments.Entities;

namespace experiments
{
    // select 

    [XmlRoot("Model")]
    public class Model
    {
        public Model()
        {
            Data = new List<Data>();
        }

        [XmlArray]
        public List<Data> Data { get; set; }
    }

    public class Data
    {
        public Data()
        {
            Fields = new List<Field>();
        }

        public string Name { get; set; }
        public DateTime Date { get; set; }
        public List<Field> Fields { get; set; }
    }

    public class Field
    {
        public string Name { get; set; }
        public decimal Value { get; set; }
    }

    public class Results
    {
        public Results()
        {
            Data = new Dictionary<DateTime, decimal>();
        }

        public Dictionary<DateTime, decimal> Data { get; set; }
    }
}